# micro:bit IoT v0.1
# Olivier Lecluse
# Novembre 2019

# Ce programme est a l'ecoute de la MB maitre

import serial
import serial.tools.list_ports as list_ports
import time
import socket
from bs4 import BeautifulSoup

# Dossier de base pages html
BASE_HTML = "/home/pi/public_html/"

# Detection automatique de la carte MB
PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 0.1

def get_ip_address():
    # Detection acces internet
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        mon_ip = s.getsockname()[0]
        s.close()
    except OSError:
        mon_ip = "Pas Internet"
    return mon_ip

def find_comport(pid, vid, baud):
    ''' return a serial port '''
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid, p.vid))
        except AttributeError:
            continue
        if (p.pid == pid) and (p.vid == vid):
            print('found target device pid: {} vid: {} port: {}'.format(
                p.pid, p.vid, p.device))
            ser_port.port = str(p.device)
            return ser_port
    return None

def modifie_html(donnee_capteur):
    # Modifie la page HTML correspondant a donnee_capteur
    dossier = donnee_capteur[0]
    no_capteur = donnee_capteur[1]
    valeur = donnee_capteur[2:]
    with open(BASE_HTML+dossier+"/index.html") as fp:
        soup = BeautifulSoup(fp,"html5lib")
        tag=soup.find("strong", id="CAP_"+dossier+no_capteur)
        if tag:
            tag.string = valeur

    html = soup.prettify("utf-8")
    with open(BASE_HTML+dossier+"/index.html", "wb") as file:
        file.write(html)

#
# Programme principal
#

while True:
    # Boucle d'attente MB
    print('Detection microbit')
    mb_serie = find_comport(PID_MICROBIT, VID_MICROBIT, 115200)
    if not mb_serie:
        print('microbit absente')
        time.sleep(1000)
    else:
        print('ouverture de la communication avec MB Maitre')
        mb_serie.open()
        
        #
        # boucle principale
        #
        
        while True:
            # Atttente d'une consigne du maitre
            data = mb_serie.readline().decode('utf-8')
            if data:
                if data == "Envoi IP":
                    # Demande d'adresse IP
                    mb_serie.write(get_ip_address().encode("utf-8"))
                elif data[0:3] == "CAP":
                    # Reception d'un capteur
                    print("Capteur ", data[3:])
                    modifie_html(data[3:])
                    # Accuse de reception
                    accuse = "ACK"+data[3:5]
                    mb_serie.write(accuse.encode("utf-8"))
                    print(accuse)
                elif data[0:5] == "Echec" :
                    # En cas d'echec, modifie la page html en consequence
                    liste = eval(data[8:])
                    print(liste)
                    for e in liste:
                        if len(e)==1:
                            for i in range(1,10):
                                modifie_html(e+str(i)+"Non connecte")
                        else:
                            modifie_html(e+"?")
