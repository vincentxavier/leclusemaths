_model: page
---
title: Les dialogues
---
date: 2020-02-23
---
body:

Revenons sur la notion de boîtes de dialogue que nous avons brièvement rencontré au début de nos activités. Dans cet exemple, nous en verrons de différents type avec des situations illustrant leur utilisation.

Le programme suivant illustre l’utilisation de 3 types de boîtes de dialogue parmi les plus courantes. Il existe plusieurs boîtes de message :
- les alertes (`messagebox`),
- celles qui demandent une info (`simpledialog`),
- celles qui permettent de choisir un fichier (`filedialog`)

```python
import tkinter as tk
from tkinter.messagebox import showinfo          # Les alertes
from tkinter.simpledialog import askstring       # demande d'infos
from tkinter.filedialog import askopenfilename   # dialogue fichiers

# Exemple avec messagebox
def bonjour():
    showinfo("Politesse","Bonjour tout le monde")
    info.config(text = "T'as vu je suis poli!")

    # Exemple avec simpledialog
def motdepasse():
    passe=askstring("Sécurité", "Quel est le mot de passe?")
    info.config(text = "Je connais ton mot de passe: "+passe)

    # Exemple avec filedialog pour ouvrir un fichier
def fichier():
    nom=askopenfilename()
    info.config(text = "Fichier choisi : "+nom)

mon_app=tk.Tk()
mon_app.title("Boîtes de message")

info=tk.Label(mon_app, text = "Ici des informations en temps réel!", font = "arial 20 bold")
info.pack()

bouton1=tk.Button(mon_app, text=" Formule de politesse !", width=20, font = "arial 20 bold",
    bg = 'yellow',fg = 'red', comman
= bonjour)
bouton1.pack()

bouton2=tk.Button(mon_app,text=" Mot de passe",width=20,font="arial 20 bold",
    bg = 'yellow', fg = 'red', command = motdepasse)
bouton2.pack()

bouton3 = tk.Button(mon_app,text=" Ouvrir Fichier",width=20,font="arial 20 bold",
bg='yellow',fg='red',command = fichier)
bouton3.pack()

mon_app.mainloop()
```

Vous avez-vu avec quelle simplicité ces dialogues se manipulent. Pour chaque type de dialogue, voici les méyhodes les plus utiles :
- Méthodes de `messagebox : `showinfo`, `showwarning`, `showerror`, `askquestion`, `askokcancel`, `askyesno`, `askretrycancel`
- Méthodes de `simpledialog` : `askstring`, `askinteger` et `askfloat`
- Méthodes de `filedialog` : `askopenfilename` et `asksaveasfilename`

## Dialogue de choix de couleur

Il existe un autre type de dialogue destiné à choisir une couleur. Voici comment il fonctionne :

```python
import tkinter as tk
from tkinter.colorchooser import askcolor

# Affiche la fenetre de sélection de couleur du système
def choix():
    result = askcolor(color = "#FFEEDD", title = "Choix de couleur")
    # La réponse est un couple avec deux formats de codage :
    # ((R,G,B) , #Hexadecimal)
    resultat.set(str(result))

# Création de la fenetre proncipale
mon_app = tk.Tk()
mon_app.title("Choix de couleur")
# Création des boutons
resultat = tk.StringVar()
tk.Label(mon_app, textvariable = resultat).pack(side = tk.BOTTOM)

tk.Button(mon_app, text = 'Choisir', fg="darkgreen", command = choix).pack(side = tk.LEFT, padx = 10)
tk.Button(mon_app, text = 'Quitter', command = mon_app.destroy,fg="red").pack(side = tk.RIGHT, padx = 10)

mon_app.mainloop()
```

## A vous de jouer

Reprendre l'application dessin réalisée précédemment et ajouter un bouton permettant de choisir la couleur avec laquelle vous allez dessiner.

Au moment de quitter, demander un message de confirmation avant de quitter.

Indication : pour récupérer la couleur sous forme exploitable par TKinter, vous exploiterez la réponde du dialogue sous la forme (R,V,B) avec une ligne du type :

    couleur="#%02x%02x%02x" % (128, 192, 200)

Voici le résultat attendu :
![](https://i.postimg.cc/mgJ6TzRV/Capture-du-2019-10-21-10-18-51.png)

```python
import tkinter as tk
from tkinter.colorchooser import askcolor
from tkinter.messagebox import askokcancel

# Constantes
LARGEUR = 480
HAUTEUR = 320

# Variables globales
old_x, old_y = 0, 0
couleur = "#%02x%02x%02x" % (128, 192, 200)

def clic(event):
    """ Gestion de l'événement clic gauche sur la zone graphique """
    global old_x, old_y
    # position du pointeur de la souris
    old_x = event.x
    old_y = event.y

def drag(event):
    global old_x, old_y
    x = event.x
    y = event.y
    surface_dessin.create_line(old_x, old_y ,x, y, fill=couleur)
    
    old_x = event.x
    old_y = event.y

def effacer():
    """ Efface la zone graphique """
    surface_dessin.delete(tk.ALL)

def choix_couleur():
    global couleur
    result = askcolor(color="#FFEEDD", title = "Choix de couleur")
    couleur = result[1]

def quitter():
    res = askokcancel("Quitter l'application","Etes vous sur ?")
    if res:
        mon_app.quit()
        mon_app.destroy()

# Création de la fenêtre principale (main window)
mon_app = tk.Tk()
mon_app.title('Dessin')

# Création d'un widget Canvas (zone graphique)
surface_dessin = tk.Canvas(mon_app, width = LARGEUR, height = HAUTEUR, bg = 'white')
surface_dessin.pack(padx =5, pady =5)


# La méthode bind() permet de lier un événement avec une fonction :
# un clic gauche sur la surface provoquera l'appel de la fonction clic()
surface_dessin.bind('<Button-1>', clic)
surface_dessin.bind('<B1-Motion>', drag)
surface_dessin.pack(padx = 5, pady = 5)

# Création d'un widget Button (bouton effacer)
tk.Button(mon_app, text = 'Effacer', command = effacer).pack(side = tk.LEFT,padx = 5, pady = 5)
# Création d'un widget Button (bouton Couleur)
tk.Button(mon_app, text ='Couleur', command = choix_couleur).pack(side = tk.LEFT, padx = 5, pady = 5)
# Création d'un widget Button (bouton Quitter)
tk.Button(mon_app, text = 'Quitter', command = quitter).pack(side = tk.LEFT)

mon_app.mainloop()
```