_model: page
---
title: TP3 : Protocole de communication

---
date: 2020-03-04
---
toc: 2
---
body:

Nous souhaitons mettre en place un *réseau de capteurs de température* basé sur la carte microbit pour relever les températures
dans plusieurs pièces d'une maison. Les températures relevées seront envoyées par liaison radio à une carte **microbit principale** qui
centralisera les informations.

Dans ce TP, nous nous intéresserons au protocole de communication entre les cartes **microbit capteur** et la carte **microbit principale**.

## Notion de protocole de communication
 
**Communiquer** consiste à transmettre des informations entre plusieurs interlocuteurs. Pour se comprendre lLes interlocuteurs doivent donc non seulement parler la même langue,
mais aussi adopter des règles communes de communication (parler à tour de rôle, choisir qui parle en premier, comment est distribuée la parole...).
C'est le rôle d'un protocole de s'assurer de tout cela. 

Prenons l'exemple, d'un appel téléphonique :
0. L'interlocuteur apprend que vous avez quelque chose à transmettre (vous composez son numéro pour faire sonner son combiné) ;
0. il indique qu'il est prêt à recevoir (il décroche et dit « Allô ») ;
0. il situe votre communication dans son contexte (« Je suis Marc. Je t'appelle pour la raison suivante… ») ;
0. les interlocuteurs échangent alors les informations 
0. pour finir, les interlocuteurs se mettent d'accord sur la fin de la communication (« Au revoir »).

Ces règles communes de communication est un exemple de protocole utilisé pour la communication téléphonique. 

## Première approche :

On considère le programme suivant sur la carte **microbit capteur** :
```python
import microbit as mb
import radio

radio.on()
radio.config(group=1)

def envoi_temperature():
    """Envoi de la temperature par radio"""
    temp = mb.temperature()
    # On transforme la temperature en texte
    # pour envoi par radio
    message = str(temp)
    radio.send(message)

#
# Boucle principale
#

while True:
    envoi_temperature()
    # Attente de une seconde
    mb.sleep(1000)
```

et sur la carte **microbit principale** :
```python
import microbit as mb
import radio

# Initialisation des communications
radio.on()
radio.config(group=1)

def lecture_radio():
    """Lecture signaux radio
    en provenance des capteurs MB"""
    radio_recu = radio.receive()
    if radio_recu:
        print(radio_recu)

#
# Boucle principale
#

while True:
    lecture_radio()
```

Décrire le protocole de communication entre les deux cartes ? Quels inconvénients peut-on anticiper ?

## version 2
### Le programme 
On met le programme suivant sur la carte **microbit capteur** :

```python
import microbit as mb
import radio

radio.on()
radio.config(group=1)

def envoi_temperature():
    """Envoi de la temperature par radio"""
    temp = mb.temperature()
    # On transforme la temperature en texte
    # pour envoi par radio
    message = "CAPA1" + str(temp)
    radio.send(message)

#
# Boucle principale
#

while True:
    envoi_temperature()
    # Attente de une seconde
    mb.sleep(1000)
```
### Questions 

On suppose que la température du capteur est de **22,6°**. 
0. Quel est le message qui sera envoyé par radio ?
0. Quelle modification a été appliquée au protocole de communication ?
0. Expliquer en quoi ce nouveau Protocole améliore certains problèmes de la première version
0. Quels problèmes peut-on encore anticiper ? 


## Espionnage du protocole utilisé

Dans la version définitive, le protocole a encore été enrichi afin de fiabiliser davantage la communication entre la **microbit principale** et les cartes **microbit capteur**. 
Néanmoins, nous n'avons pas accès au code *python* implanté dans les cartes. 
L'objectif dans cette question sera de reconstituer le protocole de communication entre la **microbit principale** et les capteurs 
en espionnant les communications.

Voici le programme espion. Implantez-le sur une des cartes et observez les communications qui circulent.
```python
# microbit IoT v0.2
# Olivier Lecluse
# Novembre 2019

# Ce programme espionne toutes les
# communications radio

import microbit as mb
import radio

radio.on()
radio.config(group=2)

def lecture_radio():
    """Lecture des consignes par radio"""
    radio_recu = radio.receive()
    if radio_recu:
        print(radio_recu)
        
while True:
    lecture_radio()
```

Décrivez le protocole de communication utilisé pour le dialogue entre les cartes **microbit**.

## Mise en place de ce protocole

On considère le programme suivant, que l'on implémente sur la carte **microbit principale** :

```python
import microbit as mb
import radio

# Initialisation des communications
radio.on()
radio.config(group=1)

def envoi_requete(nom_capteur):
    radio.send("REQ"+nom_capteur)

def lecture_radio():
    """Lecture signaux radio
    en provenance des capteurs MB"""
    
    radio_recu = ""
    while not radio_recu :
        radio_recu = radio.receive()
    return radio_recu

#
# Boucle principale
#

while True:
    envoi_requete("A")
    ta = lecture_radio()
    print("Temperature A : ", ta)
    mb.sleep(1000)

    envoi_requete("B")
    tb = lecture_radio()
    print("Temperature B : ", tb)
    mb.sleep(1000)
```

Ecrire le programme correspondant destiné à la carte **microbit capteur "A"**
